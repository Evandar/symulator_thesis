#include "heap.h"

Vertex::Vertex(int inputX, int inputY, int inputCost)
{
    x = inputX;
    y = inputY;
    cost = inputCost;
}

void Heap::Push(Vertex v)
{
    int at = size++;
    data[at] = v;
    while(at > 0 && data[at].cost < data[at/2].cost)
    {
        Vertex temp = data[at];
        data[at] = data[at/2];
        data[at/2] = temp;
        at /= 2;
    }
}

Vertex Heap::Pop()
{
    Vertex ret = data[0];
    data[0] = data[--size];

    int at = 0;
    while(1)
    {
        int son = 2*at + 1;
        if(son >= size) break;
        if(son+1 < size && data[son].cost > data[son+1].cost) son++;
        if(data[at].cost > data[son].cost)
        {
            Vertex temp = data[at];
            data[at] = data[son];
            data[son] = temp;
            at = son;
        }
        else
            break;
    }

    return ret;
}

