#ifndef ACTIONQ_H
#define ACTIONQ_H

#include <queue>
#include <main.h>

enum ActionStatusNum
{
    DONE,
    TODO,
    PROCESSING,
};

enum ActionTypeNum
{
    MOVE,
    PIVOT,
    PIVOT_180,
    // TURN_SMOOTH,
    CALIBRATE,
};

struct ActionPack
{
    ActionPack()
    {
        status = TODO;
        type = MOVE;
        arg = 0;
    }

    ActionStatusNum status;
    ActionTypeNum type;
    double arg;
};

class ActionQ
{
public:
    ActionQ();

    ActionPack aQ[ACTIONQ_SIZE];
    int qR;
    int qW;

    void AddMove(int arg);
    void AddTurn(int arg);
    void Process();
    void Clean();
    bool Status();
};

#endif // ACTIONQ_H
