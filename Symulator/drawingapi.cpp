#include "drawingapi.h"

using namespace std;

DrawingAPI::DrawingAPI(Maze *inputMaze, double scale)
{
    maze = inputMaze;
    winScale = scale;

    mazeImg = Mat(maze->size * ONE_FIELDPX * winScale + SHORTSEG * winScale,
                  maze->size * ONE_FIELDPX * winScale + SHORTSEG * winScale, CV_8UC3);

    if(maze->mouse != NULL)
    {
        robot = imread("../img/Mouse.png");
        resize(robot, robot, Size(LONGSEG, LONGSEG));
    }

}

int DrawingAPI::GetCoord(int i)
{
    return i/2 * LONGSEG * winScale + i/2 * SHORTSEG * winScale  + (i%2) * SHORTSEG * winScale;
}

int DrawingAPI::GetCenter(int i)
{
    return (GetCoord(i) + GetCoord(i+1)-1) / 2;
}

void DrawingAPI::Draw(int i, int j, Scalar color)
{
    int xBeg = GetCoord(i);
    int yBeg = GetCoord(j);
    int xEnd = GetCoord(i+1)-1;
    int yEnd = GetCoord(j+1)-1;

    rectangle(mazeImg, Point(xBeg, yBeg), Point(xEnd, yEnd), color, -1);
}

WallNum DrawingAPI::GetWall(int x)
{
    if(x & CHECKED)
    {
        if(x & WALL)
            return Wall;
        else
            return noWall;
    }
    return Unknown;
}

Mat DrawingAPI::RotateRobot(int dir)
{
    Mat temp = robot.clone();

    switch(dir)
    {
        case N:
            break;
        case E:
            transpose(temp, temp);
            flip(temp, temp, 0);
            break;
        case S:
            flip(temp, temp, -1);
            break;
         case W:
            transpose(temp, temp);
            flip(temp, temp, 1);
        break;
    }

    return temp;
}

void DrawingAPI::Update()
{
    for(int i=0; i<maze->sizeCal; i++)
    {
        for(int j=0; j<maze->sizeCal; j++)
        {
            switch(GetWall(maze->map[i][j]))
            {
                case Wall:
                    Draw(i,j, palette.white);
                    break;
                case noWall:
                    Draw(i,j, palette.black);
                    break;
                case Unknown:
                    Draw(i,j, palette.gray);
                    break;
            }
            if(maze->map[i][j] & VMOUSE)
                Draw(i,j, palette.darkGreen);

            if(maze->map[i][j] & FASTEST_TRACK)
            {
                rectangle(mazeImg, cvPoint(GetCoord(i)+SHORTSEG * winScale, GetCoord(j)+SHORTSEG * winScale), cvPoint(GetCoord(i) + LONGSEG * winScale - SHORTSEG * winScale, GetCoord(j) + LONGSEG * winScale - SHORTSEG * winScale), palette.red, -1);
            }

            if(maze->map[i][j] & TRACK)
            {
                rectangle(mazeImg, cvPoint(GetCoord(i)+2*SHORTSEG * winScale, GetCoord(j)+2*SHORTSEG * winScale), cvPoint(GetCoord(i) + LONGSEG * winScale - 2*SHORTSEG * winScale, GetCoord(j) + LONGSEG * winScale - 2*SHORTSEG * winScale), palette.blue, -1);
            }
            if (!((i&1) || (j&1)))              //slupki
            {
                if(!(i == INIT_ENDX && j == INIT_ENDY)) Draw(i,j,palette.red);
            }
        }
    }

    if(maze->mouse)
    {
        int tempX = GetCenter(maze->mouse->posX);
        int tempY = GetCenter(maze->mouse->posY);
        int z = LONGSEG * winScale/2;

        Rect roi(tempX - z + 1, tempY-z + 1, LONGSEG * winScale, LONGSEG * winScale);
        Mat destinationROI = mazeImg( roi );
        RotateRobot(maze->mouse->orientation).copyTo( destinationROI );
    }

    flip(mazeImg, mazeImg, 0);
    cvtColor(mazeImg, mazeImg, CV_BGR2RGB);
}
