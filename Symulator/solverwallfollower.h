#ifndef SOLVERWALLFOLLOWER_H
#define SOLVERWALLFOLLOWER_H

#include <solver.h>
#include <maze.h>

typedef enum
{
    LEFT,
    RIGHT,
}WallFollowNum;


class SolverWallFollower : public Solver
{
public:
    SolverWallFollower(Maze *inputMaze, WallFollowNum dir);
    ~SolverWallFollower()
    {

    }

    Maze *maze;
    WallFollowNum followDir;
    int avMoves[8] = {0,1,-1,0,0,-1,1,0};
    bool ex;
    bool ComputeAlgorithm();

    bool LeftField();
    bool FrontField();
    bool RightField();
    void OptimalRoute();
};

#endif // SOLVERWALLFOLLOWER_H
