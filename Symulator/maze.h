#ifndef MAZE_H
#define MAZE_H
#include <main.h>
#include <mouse.h>

class Maze
{
public:
    Maze(int inputSize);
    Maze(int inputSize, Mouse *inputMouse);

    int size;
    int sizeCal;
    int **map;
    Mouse *mouse;


    void Init(int inputSize);
    void Read();
    void WriteOut();
    void MarkVisited();
    void RemoveMouse(int posX, int posY);
    void SetMouse(int posX, int posY);
};

#endif // MAZE_H
