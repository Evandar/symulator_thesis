#include "actionq.h"

ActionQ::ActionQ()
{
    qR = 0;
    qW = 0;
}

void ActionQ::AddMove(int inputArg)
{
    ActionPack temp;
    temp.type = MOVE;
    temp.arg = inputArg;

    aQ[qW] = temp;

    qW++;
    if(qW == ACTIONQ_SIZE)
        qW = 0;
}

void ActionQ::AddTurn(int inputArg)
{
    ActionPack temp;
    temp.type = PIVOT;
    temp.arg = inputArg;

    aQ[qW] = temp;

    qW++;
    if(qW == ACTIONQ_SIZE)
        qW = 0;
}

void ActionQ::Clean()
{
    for(int i = 0; i < ACTIONQ_SIZE; i++)
    {
        aQ[i].status = DONE;
    }
    qW = 0;
    qR = 0;
}

bool ActionQ::Status()
{
    if(qW == qR)
        return 1;
    else
        return 0;
}
