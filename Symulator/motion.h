#ifndef MOTION_H
#define MOTION_H

void Move();
void Turn(int x);
bool WallCheck(int x, int y);
void AddMove();
void AddTurn(int x);
void ActionQProcess();
void ActionQClean();
bool ActionQStatus();

extern int posX;
extern int posY;
extern int endX;
extern int endY;
extern int startX;
extern int startY;
extern int cutX;
extern int cutY;


#endif // MOTION_H
