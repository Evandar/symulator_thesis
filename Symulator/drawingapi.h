#ifndef DRAWINGAPI_H
#define DRAWINGAPI_H


#include <opencv2/opencv.hpp>
#include <main.h>
#include <maze.h>
#include <mouse.h>
#include <colors.h>

using namespace cv;



class DrawingAPI
{
public:
    DrawingAPI(Maze *inputMaze, double scale);


    Maze *maze;
    Colors palette;

    Mat mazeImg;
    Mat robot;

    double winScale;
    void Update();
    int GetCoord(int i);
    int GetCenter(int i);
    void Draw(int i, int j, Scalar color);
    WallNum GetWall(int x);
    Mat RotateRobot(int dir);
};

#endif // DRAWINGAPI_H
