#ifndef SOLVER_H
#define SOLVER_H

#include <mouse.h>
class Solver
{
public:
    Solver();
    virtual ~Solver(){

    }
    virtual bool ComputeAlgorithm() = 0;
    virtual void OptimalRoute() = 0;

    int Prev(PII x);
    PII Prev(int x);
    bool Goal(Mouse *mouse);
    bool Goal(int x, int y, Mouse *mouse);
    bool GoalEx(Mouse *mouse);
    bool GoalEx(int x, int y, Mouse *mouse);
    void ShowFastest(int x, int y);
};

#endif // SOLVER_H
