#include "colors.h"


Colors::Colors()
{
    white = cv::Scalar(255,255,255);
    black = cv::Scalar(0,0,0);
    gray = cv::Scalar(100,100,100);
    red = cv::Scalar(100,0,255);
    blue = cv::Scalar(255,0,100);
    darkGreen = cv::Scalar(34,139,34);
}
