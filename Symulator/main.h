#ifndef MAIN_H
#define MAIN_H


#define MAZE_SIZE 16
#define LONGSEG 30
#define SHORTSEG 3
#define ONE_FIELDPX (LONGSEG + SHORTSEG)
#define TURN_WEIGHT 3
#define BACK_WEIGHT 11
#define STRAIGHT_WEIGHT 1
#define ACTIONQ_SIZE 1100
#define BFS_Q_SIZE 300
#define HEAP_SIZE 1100
#define RANGE 5
#define INIT_STARTX 1
#define INIT_STARTY 1
#define INIT_ENDX 16
#define INIT_ENDY 16

#define MAZE_SIZE_CAL MAZE_SIZE * 2 + 1

#define FASTEST_TRACK 64
#define VMOUSE 32
#define TRACK 16
#define VIS 0x08
#define MOUSE 0x04
#define WALLCHECKED 0x03
#define WALL 0x02
#define CHECKED 0x01

#define DIR_N 0b00000001
#define DIR_E 0b00000100
#define DIR_S 0b00010000
#define DIR_W 0b01000000

#define ST first
#define ND second
#define INF 99999
#include <vector>
#include <iostream>
typedef std::pair<int,int> PII;

typedef enum
{
    noWall,
    Wall,
    Unknown
}WallNum;


typedef enum
{
    WALLFOLLOWERL,
    WALLFOLLOWERR,
    DFS,
    BFS,
    ASTAR,
    ASTARIMP,
}MethodNum;


PII SubPII(PII a, PII b);
double Dot(PII u, PII v);
double Det(PII u, PII v);

#endif // MAIN_H
