#ifndef HEAP_H
#define HEAP_H

#include <main.h>

class Vertex
{
public:
    int x;
    int y;
    int cost;

    Vertex(int inputX=0, int inputY=0, int inputCost=0);
};

class Heap
{
public:
    Heap(){
        size = 0;
    }

    Vertex data[HEAP_SIZE];
    int size;

    void Push(Vertex v);
    Vertex Pop();
};

#endif // HEAP_H
