#ifndef MOUSE_H
#define MOUSE_H

#include <main.h>
#include <actionq.h>


typedef enum
{
    N = 0,
    NW,
    W,
    SW,
    S,
    SE,
    E,
    NE
}OrientationNum;


class Mouse
{
public:
    Mouse(int sX, int sY, int eX, int eY, OrientationNum orient);

    ActionQ actionQ;

    OrientationNum orientation;
    int startX;
    int startY;
    int endX;
    int endY;
    int posX;
    int posY;
    int gX;
    int gY;

    int steps;
    int moves;
    int turns;
    int optimalCost;

    void SetGoal(int eX, int eY);

    void ProcessAQ();
    void ProcessMove(int arg);
    void ProcessTurn(int arg);
};

#endif // MOUSE_H
