#include "solverwallfollower.h"
#include <iostream>

using namespace std;


SolverWallFollower::SolverWallFollower(Maze *inputMaze, WallFollowNum dir)
{
    maze = inputMaze;
    followDir = dir;
    ex = 0;
}


bool SolverWallFollower::ComputeAlgorithm()
{

    if(Goal(maze->mouse)) return 1;
    if(maze->mouse->startX == maze->mouse->posX && maze->mouse->startY == maze->mouse->posY)
    {
        if(ex == 1)
        {
            maze->mouse->steps = -1;
            return 1;
        }
        ex = 1;
    }

    if((followDir == LEFT)? LeftField() : RightField())
    {
        maze->mouse->actionQ.AddTurn((followDir == LEFT)? 2 : -2);
        maze->mouse->actionQ.AddMove(1);
        maze->mouse->actionQ.AddMove(1);
    }
    else if (FrontField())
    {
        maze->mouse->actionQ.AddMove(1);
        maze->mouse->actionQ.AddMove(1);
    }
    else if ((followDir == LEFT)? RightField() : LeftField())
    {
        maze->mouse->actionQ.AddTurn((followDir == LEFT)? -2 : 2);
        maze->mouse->actionQ.AddMove(1);
        maze->mouse->actionQ.AddMove(1);
    }
    else
    {
        maze->mouse->actionQ.AddTurn(4);
    }

    return 0;
}


bool SolverWallFollower::LeftField()
{
    int temp = maze->mouse->orientation;
    temp += 2;
    if(temp >= 8) temp -= 8;

    int field = maze->map[maze->mouse->posX + avMoves[temp]][maze->mouse->posY + avMoves[temp + 1]];

    if(!(field & WALL))
        return 1;
    return 0;
}


bool SolverWallFollower::FrontField()
{
    int temp = maze->mouse->orientation;

    int field = maze->map[maze->mouse->posX + avMoves[temp]][maze->mouse->posY + avMoves[temp + 1]];

    if(!(field & WALL))
        return 1;
    return 0;
}


bool SolverWallFollower::RightField()
{
    int temp = maze->mouse->orientation;
    temp -= 2;
    if(temp < 0) temp += 8;

    int field = maze->map[maze->mouse->posX + avMoves[temp]][maze->mouse->posY + avMoves[temp + 1]];

    if(!(field & WALL))
        return 1;
    return 0;
}

void SolverWallFollower::OptimalRoute()
{

}
