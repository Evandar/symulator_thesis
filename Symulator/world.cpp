#include <mainwindow.h>
#include <symulator.h>
int cordsNS[10] = {0,1, 0,2, 1,2, 2,2, 2,3};
int cordsEW[10] = {1,0, 2,0, 2,1, 2,2, 3,2};


bool MainWindow::CheckN(int x, int y, int special)
{
    int ex = 0;
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze->map[x][y+i] & CHECKED))
            ex = 1;
        maze->map[x][y+i] |= mazeFull->map[x][y+i];
        maze->map[x][y+i] |= CHECKED;
        if(mazeFull->map[x][y+i] == WALLCHECKED)
        {
            break;
        }
    }

    if(special)
    {
        for(int i=0; i<10; i+=2)
        {

            int tox = x + cordsNS[i];
            int toy = y + cordsNS[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
            {
                break;
            }

        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x - cordsNS[i];
            int toy = y + cordsNS[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
            {
                break;
            }

        }
    }
    return ex;
}

bool MainWindow::CheckE(int x, int y, int special)
{
    bool ex = 0;

    // glowny
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze->map[x+i][y] & CHECKED))
            ex = 1;
        maze->map[x+i][y] |= mazeFull->map[x+i][y];
        maze->map[x+i][y] |= CHECKED;
        if(mazeFull->map[x+i][y] == WALLCHECKED)
        {
            break;
        }
    }

    if(special)
    {
        for(int i=0; i<10; i+=2)
        {
            int tox = x + cordsEW[i];
            int toy = y + cordsEW[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
            {
                break;
            }
        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x + cordsEW[i];
            int toy = y - cordsEW[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
            {
                break;
            }
        }
    }
    return ex;
}

bool MainWindow::CheckS(int x, int y, int special)
{
    bool ex = 0;
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze->map[x][y-i] & CHECKED))
            ex = 1;
        maze->map[x][y-i] |= mazeFull->map[x][y-i];
        maze->map[x][y-i] |= CHECKED;
        if(mazeFull->map[x][y-i] == WALLCHECKED)
            break;
    }

    if(special)
    {
        for(int i=0; i<10; i+=2)
        {

            int tox = x - cordsNS[i];
            int toy = y - cordsNS[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
                break;

        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x + cordsNS[i];
            int toy = y - cordsNS[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
                break;

        }
    }
    return ex;
}

bool MainWindow::CheckW(int x, int y, int special)
{
    bool ex = 0;
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze->map[x-i][y] & CHECKED))
            ex = 1;
        maze->map[x-i][y] |= mazeFull->map[x-i][y];
        maze->map[x-i][y] |= CHECKED;

        if(mazeFull->map[x-i][y] == WALLCHECKED)
            break;
    }
    if(special)
    {
        for(int i=0; i<10; i+=2)
        {
            int tox = x - cordsEW[i];
            int toy = y + cordsEW[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
                break;
        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x - cordsEW[i];
            int toy = y - cordsEW[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
                break;
        }
    }
    return ex;
}

bool MainWindow::WallCheck(int x, int y)
{
    maze->map[x][y] |= CHECKED;

    bool ex = 0;
    switch(mouse->orientation)
    {
        case N:
            ex += CheckN(x,y,1);
            ex += CheckW(x,y,0);
            ex += CheckE(x,y,0);
            break;

        case E:
            ex += CheckE(x,y,1);
            ex += CheckN(x,y,0);
            ex += CheckS(x,y,0);
            break;

        case S:
            ex += CheckS(x,y,1);
            ex += CheckE(x,y,0);
            ex += CheckW(x,y,0);
            break;

        case W:
            ex += CheckW(x,y,1);
            ex += CheckN(x,y,0);
            ex += CheckS(x,y,0);
            break;
    }

    return ex;
}


bool Symulator::CheckN(int x, int y, int special)
{
    int ex = 0;
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze->map[x][y+i] & CHECKED))
            ex = 1;
        maze->map[x][y+i] |= mazeFull->map[x][y+i];
        maze->map[x][y+i] |= CHECKED;
        if(mazeFull->map[x][y+i] == WALLCHECKED)
        {
            break;
        }
    }

    if(special)
    {
        for(int i=0; i<10; i+=2)
        {

            int tox = x + cordsNS[i];
            int toy = y + cordsNS[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
            {
                break;
            }

        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x - cordsNS[i];
            int toy = y + cordsNS[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
            {
                break;
            }

        }
    }
    return ex;
}

bool Symulator::CheckE(int x, int y, int special)
{
    bool ex = 0;

    // glowny
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze->map[x+i][y] & CHECKED))
            ex = 1;
        maze->map[x+i][y] |= mazeFull->map[x+i][y];
        maze->map[x+i][y] |= CHECKED;
        if(mazeFull->map[x+i][y] == WALLCHECKED)
        {
            break;
        }
    }

    if(special)
    {
        for(int i=0; i<10; i+=2)
        {
            int tox = x + cordsEW[i];
            int toy = y + cordsEW[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
            {
                break;
            }
        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x + cordsEW[i];
            int toy = y - cordsEW[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
            {
                break;
            }
        }
    }
    return ex;
}

bool Symulator::CheckS(int x, int y, int special)
{
    bool ex = 0;
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze->map[x][y-i] & CHECKED))
            ex = 1;
        maze->map[x][y-i] |= mazeFull->map[x][y-i];
        maze->map[x][y-i] |= CHECKED;
        if(mazeFull->map[x][y-i] == WALLCHECKED)
            break;
    }

    if(special)
    {
        for(int i=0; i<10; i+=2)
        {

            int tox = x - cordsNS[i];
            int toy = y - cordsNS[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
                break;

        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x + cordsNS[i];
            int toy = y - cordsNS[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
                break;

        }
    }
    return ex;
}

bool Symulator::CheckW(int x, int y, int special)
{
    bool ex = 0;
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze->map[x-i][y] & CHECKED))
            ex = 1;
        maze->map[x-i][y] |= mazeFull->map[x-i][y];
        maze->map[x-i][y] |= CHECKED;

        if(mazeFull->map[x-i][y] == WALLCHECKED)
            break;
    }
    if(special)
    {
        for(int i=0; i<10; i+=2)
        {
            int tox = x - cordsEW[i];
            int toy = y + cordsEW[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
                break;
        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x - cordsEW[i];
            int toy = y - cordsEW[i+1];

            if(!(maze->map[tox][toy] & CHECKED))
                ex = 1;
            maze->map[tox][toy] |= mazeFull->map[tox][toy];
            maze->map[tox][toy] |= CHECKED;
            if(mazeFull->map[tox][toy] == WALLCHECKED)
                break;
        }
    }
    return ex;
}

bool Symulator::WallCheck(int x, int y)
{
    maze->map[x][y] |= CHECKED;

    bool ex = 0;
    switch(mouse->orientation)
    {
        case N:
            ex += CheckN(x,y,1);
            ex += CheckW(x,y,0);
            ex += CheckE(x,y,0);
            break;

        case E:
            ex += CheckE(x,y,1);
            ex += CheckN(x,y,0);
            ex += CheckS(x,y,0);
            break;

        case S:
            ex += CheckS(x,y,1);
            ex += CheckE(x,y,0);
            ex += CheckW(x,y,0);
            break;

        case W:
            ex += CheckW(x,y,1);
            ex += CheckN(x,y,0);
            ex += CheckS(x,y,0);
            break;
    }

    return ex;
}
