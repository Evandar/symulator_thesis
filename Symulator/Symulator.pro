TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
#CONFIG -= qt


QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


SOURCES += main.cpp \
#    utils.cpp \
#    dfs.cpp \
#    motion.cpp \
    maze.cpp \
    drawingapi.cpp \
    colors.cpp \
    mouse.cpp \
    actionq.cpp \
    symulator.cpp \
    world.cpp \
    solver.cpp \
    solverwallfollower.cpp \
    solverbfs.cpp \
    heap.cpp \
    solverastar.cpp \
    solverastarimp.cpp \
    mainwindow.cpp
    solverastar.cpp

HEADERS += \
    main.h \
#    utils.h \
#    motion.h \
    maze.h \
    drawingapi.h \
    colors.h \
    mouse.h \
    actionq.h \
    symulator.h \
    solver.h \
    solverwallfollower.h \
    solverbfs.h \
    heap.h \
    solverastar.h \
    solverastarimp.h \
    mainwindow.h
    solverastar.h


INCLUDEPATH += /usr/local/include/opencv
LIBS += `pkg-config --libs opencv` #-L/usr/local/lib -lopencv_core -lopencv_highgui

FORMS += \
    mainwindow.ui

DISTFILES +=
