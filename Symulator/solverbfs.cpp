#include "solverbfs.h"
#include <iomanip>

using namespace std;

double Dot(int x1, int y1, int x2, int y2)
{
    return x1 * x2 + y1 * y2;
}

int Det(int x1, int y1, int x2, int y2)
{
    return x1 * y2 - x2 * y1;
}


SolverBFS::SolverBFS(Maze *inputMaze)
{
    maze = inputMaze;

    prev = new PII*[maze->sizeCal];
    for(int i = 0; i < maze->sizeCal; i++)
        prev[i] = new PII[maze->sizeCal];

    returnStatus = 0;

    qW = 0;
    qR = 0;
    av = 1;
    tempOrient = 0;
}


void SolverBFS::Clean()
{
    for(int i = 0; i < maze->sizeCal; i++)
    {
        for(int j = 0; j < maze->sizeCal; j++)
        {
            prev[i][j] = PII(-1,-1);
            maze->map[i][j] &= ~TRACK;
            maze->map[i][j] &= ~FASTEST_TRACK;
        }
    }
    qR = 0;
    qW = 0;
    tempOrient = maze->mouse->orientation;
    av=1;
}

bool SolverBFS::ComputeAlgorithm()
{
    Clean();

    if(returnStatus)
    {
        if(GoalEx(maze->mouse))
        {
            maze->mouse->gX = maze->mouse->endX;
            maze->mouse->gY = maze->mouse->endY;

            BFS(maze->mouse->startX, maze->mouse->startY);
            returnStatus = 0;
            ShowFastest(maze->mouse->endX, maze->mouse->endY);
            return CheckPath(maze->mouse->endX, maze->mouse->endY);
        }
    }
    else if(Goal(maze->mouse))
    {
        maze->mouse->gX = maze->mouse->startX;
        maze->mouse->gY = maze->mouse->startY;
        returnStatus = 1;
    }


    BFS(maze->mouse->posX, maze->mouse->posY);
    ShowFastest(maze->mouse->gX, maze->mouse->gY);
    DetermineMoves(0, maze->mouse->gX, maze->mouse->gY, tempOrient);

    return 0;
}


void SolverBFS::BFS(int x, int y)
{
    q[qW++] = PII(x, y);
    prev[x][y] = PII(x, y);


    while(qW != qR)
    {
        PII temp = q[qR++];

        if(qR == BFS_Q_SIZE)
            qR = 0;

        int x = temp.ST;
        int y = temp.ND;


        for(int i = 0; i < 8; i+=2)
        {
            int toX = x + avMoves[i];
            int toY = y + avMoves[i+1];


            if(prev[toX][toY] == PII(-1,-1) && !(maze->map[toX][toY] & WALL))
            {
                prev[toX][toY] = PII(x,y);
                q[qW++] = PII(toX,toY);

                if(qW == BFS_Q_SIZE)
                    qW = 0;
            }
        }

        if(maze->mouse->gX == x && maze->mouse->gY == y)
            break;
    }
}


void SolverBFS::DetermineMoves(bool fake, int x, int y, int &tempOrient)
{
    PII curField = PII(x,y);
    PII prevField = prev[x][y];


    if(prev[x][y] == curField)
        return;

    DetermineMoves(fake, prevField.ST, prevField.ND, tempOrient);

    if(maze->map[prevField.ST][prevField.ND] & CHECKED && av)
    {
        maze->map[curField.ST][curField.ND] |= TRACK;

        PII vecU = SubPII(curField, prevField);
        PII vecV = PII(avMoves[tempOrient], avMoves[tempOrient+1]);

        double vectorProduct = Det(vecV, vecU);
        double scalarProduct = Dot(vecV, vecU);

        if(vectorProduct == 0)
        {
            if(scalarProduct < 0)
            {
                if(!fake) maze->mouse->actionQ.AddTurn(4);
                tempOrient -= 4;
            }
        }
        if(scalarProduct == 0)
        {
            if(vectorProduct < 0)
            {
                if(!fake) maze->mouse->actionQ.AddTurn(-2);
                tempOrient-=2;
            }
            else
            {
                if(!fake) maze->mouse->actionQ.AddTurn(2);
                tempOrient+=2;
            }
            if(fake)
            {
                maze->mouse->turns++;
            }
        }

        if(!fake)
            maze->mouse->actionQ.AddMove(1);
        else
            maze->mouse->moves++;

        if(tempOrient < 0) tempOrient += 8;
        if(tempOrient >= 8) tempOrient -= 8;

    }
    else
        av = 0;
}



bool SolverBFS::CheckPath(int x, int y)
{
    PII curField = PII(x,y);
    PII prevField = prev[x][y];

    if(prev[x][y] == curField)
        return 1;

    if(maze->map[curField.ST][curField.ND] & CHECKED)
        return CheckPath(prevField.ST, prevField.ND);

    return 0;
}

void SolverBFS::ShowFastest(int x, int y)
{
    PII curField = PII(x, y);
    PII prevField = prev[x][y];

    if(prev[x][y] == curField)
        return;

    maze->map[curField.ST][curField.ND] |= FASTEST_TRACK;
    ShowFastest(prevField.ST, prevField.ND);

    return;
}

void SolverBFS::OptimalRoute()
{
    Clean();
    BFS(maze->mouse->startX, maze->mouse->startY);

    ShowFastest(maze->mouse->endX, maze->mouse->endY);
    tempOrient = maze->mouse->orientation;
    maze->mouse->moves = 0;
    maze->mouse->turns = 0;
    DetermineMoves(1, maze->mouse->endX, maze->mouse->endY, tempOrient);
}
