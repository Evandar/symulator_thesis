#include "mainwindow.h"
#include "ui_mainwindow.h"


using namespace std;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    mouse = new Mouse(1, 1, 16, 16, N);
    mazeFull = new Maze(16);

    maze = new Maze(16, mouse);
    solver = new SolverWallFollower(maze, LEFT);
    wF = 1;
    start = 0;
    pause = 0;
    waitTime = 300;

    previewAPI = new DrawingAPI(mazeFull, 1);
    realAPI = new DrawingAPI(maze, 1);


    ui->setupUi(this);
    ui->StartButton->setEnabled(false);
    ui->PauseButton->setEnabled(false);

    UpdateMat();
    UpdateSteps();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::UpdateMat()
{
    previewAPI->Update();
    realAPI->Update();

    QImage prevImg = QImage(previewAPI->mazeImg.data, previewAPI->mazeImg.cols, previewAPI->mazeImg.rows, previewAPI->mazeImg.step, QImage::Format_RGB888);
    QImage realImg = QImage(realAPI->mazeImg.data, realAPI->mazeImg.cols, realAPI->mazeImg.rows, realAPI->mazeImg.step, QImage::Format_RGB888);

    ui->prev_lab->setPixmap(QPixmap::fromImage(prevImg));
    ui->real_lab->setPixmap(QPixmap::fromImage(realImg));
}

void MainWindow::UpdateSteps()
{
    QString s1 = "Pola pokonane przez mysz: \n" + QString::number(mouse->steps);
    QString s2= "";

    if(mouse->moves)
    {
        s2 = "\nOptymalna ścieżka składa się z:\n" + QString::number(mouse->moves) + " ruchów po prostej oraz :\n"
                + QString::number(mouse->turns) + " skrętów\n";
    }

    ui->textBrowser->setText(s1 +  s2);

}

void MainWindow::on_LoadButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString(), tr("Maze Files (*.maze)"));
    if (!fileName.isEmpty())
    {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly))
            return;

//        Reset();

        QTextStream in(&file);
        for(int i=mazeFull->sizeCal-1; i>=0; i--)
            for(int j=0; j<mazeFull->sizeCal; j++)
                in >> mazeFull->map[j][i];

        mazeFull->map[mouse->endX][mouse->endY] = 0;

        file.close();
        UpdateMat();
    }
}

void MainWindow::on_ResetButton_clicked()
{
    Reset();
}

void MainWindow::on_comboBox_activated(const QString &x)
{
    delete solver;
    wF = 0;

    if(x != "---")
        ui->StartButton->setEnabled(true);

    if(x == "Left wallfollower")
    {
        solver = new SolverWallFollower(maze, LEFT);
        wF = 1;
    }
    else if(x == "Right wallfollower")
    {
        solver = new SolverWallFollower(maze, RIGHT);
        wF = 1;
    }
    else if(x == "FloodFill")
        solver = new SolverBFS(maze);
    else if(x == "Weighted FloodFill")
        solver = new SolverAStar(maze);
    else if(x == "Own FloodFill")
        solver = new SolverAStarImp(maze);

}

void MainWindow::on_speedSelector_sliderMoved(int x)
{
    waitTime = x * 10;
    if(start)
    {
        killTimer(timerId);
        timerId = startTimer(waitTime);
    }
}

void MainWindow::on_StartButton_clicked()
{

    maze->MarkVisited();
    WallCheck(mouse->posX, mouse->posY);
    solver->ComputeAlgorithm();

    timerId = startTimer(waitTime);
    ui->StartButton->setEnabled(false);
    ui->PauseButton->setEnabled(true);
    start = 1;
}


void MainWindow::on_PauseButton_clicked()
{
    if(!pause)
    {
        pause = 1;
        killTimer(timerId);
        ui->PauseButton->setText("Wznów symulację");
        UpdateMat();
    }
    else
    {
        pause = 0;
        timerId = startTimer(waitTime);
        ui->PauseButton->setText("Zatrzymaj symulację\n");
    }
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    mouse->ProcessAQ();
    maze->MarkVisited();

    if(wF)
    {
        WallCheck(mouse->posX, mouse->posY);
        if(mouse->actionQ.Status())
        {
            mouse->actionQ.Clean();
            if(solver->ComputeAlgorithm())
            {
                killTimer(timerId);
                solver->OptimalRoute();
                UpdateMat();
            }
        }
    }
    else
    {
        if(WallCheck(mouse->posX, mouse->posY) || mouse->actionQ.Status())
        {
            mouse->actionQ.Clean();
            if(solver->ComputeAlgorithm())
            {
                killTimer(timerId);
                solver->OptimalRoute();
                UpdateMat();
            }
        }
    }
    if(waitTime != 0)
        UpdateMat();

    UpdateSteps();
}


void MainWindow::Reset()
{
    killTimer(timerId);
    delete previewAPI;
    delete realAPI;

    delete mouse;
    delete maze;
    delete solver;

    mouse = new Mouse(1, 1, 16, 16, N);
    mazeFull = new Maze(16);
    maze = new Maze(16, mouse);
    solver = new SolverWallFollower(maze, LEFT);


    start = 0;
    pause = 0;
    waitTime = 300;

    previewAPI = new DrawingAPI(mazeFull, 1);
    realAPI = new DrawingAPI(maze, 1);

    UpdateMat();

    ui->StartButton->setEnabled(false);
    ui->PauseButton->setEnabled(false);
    ui->comboBox->setCurrentIndex(0);
    ui->textBrowser->setText("Pola pokonane przez mysz:\n 0\n");
}
