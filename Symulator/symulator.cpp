#include "symulator.h"

Symulator::Symulator(int size, int sX, int sY, int eX, int eY, OrientationNum orient, MethodNum method)
{
    mouse = new Mouse(sX, sY, eX, eY, orient);
    mazeFull = new Maze(size, mouse);
    maze = new Maze(size, mouse);

    switch(method)
    {
    case WALLFOLLOWERL:
        solver = new SolverWallFollower(maze, LEFT);
        break;
    case WALLFOLLOWERR:
        solver = new SolverWallFollower(maze, RIGHT);
        break;
    case BFS:
        solver = new SolverBFS(maze);
        break;
    case ASTAR:
        solver = new SolverAStar(maze);
        break;
    case ASTARIMP:
        solver = new SolverAStarImp(maze);
        break;
    }
}


void Symulator::Brute()
{
    WallCheck(mouse->posX, mouse->posY);
    solver->ComputeAlgorithm();

    while(1)
    {
        mouse->ProcessAQ();
        WallCheck(mouse->posX, mouse->posY) ;
        if(mouse->actionQ.Status())
        {
            mouse->actionQ.Clean();
            if(solver->ComputeAlgorithm())
                break;
        }
    }
}


bool Symulator::SearchRun()
{
    maze->MarkVisited();
    WallCheck(mouse->posX, mouse->posY);
    solver->ComputeAlgorithm();

    while(1)
    {
        mouse->ProcessAQ();
        maze->MarkVisited();
        if(WallCheck(mouse->posX, mouse->posY) || mouse->actionQ.Status())
        {
            mouse->actionQ.Clean();
            if(solver->ComputeAlgorithm())
                break;
        }
    }

    return 1;
}


