#include "maze.h"
#include <iostream>

using namespace std;


Maze::Maze(int inputSize)
{
    mouse = NULL;
    Init(inputSize);
}

Maze::Maze(int inputSize, Mouse *inputMouse)
{
    mouse = inputMouse;
    Init(inputSize);
}

void Maze::Init(int inputSize)
{
    size = inputSize;
    sizeCal = 2 * size + 1;

    map = new int*[sizeCal];
    for(int i = 0; i < sizeCal; i++)
    {
        map[i] = new int[sizeCal];
    }

    for(int i=0; i<sizeCal; i++)
    {
        for(int j=0; j<sizeCal; j++)
        {
            map[i][j] &= !CHECKED;
            if (!((i&1) || (j&1)))              //slupki
                map[i][j] = WALLCHECKED;
        }
    }

    map[INIT_ENDX][INIT_ENDY] = 0;

    for(int i=0; i<sizeCal; i++)
    {
        map[0][i] = map[i][0] = WALLCHECKED;
        map[sizeCal-1][i] = map[i][sizeCal-1] = WALLCHECKED;
    }

    if(mouse != NULL)
    {
        map[2][1] |= WALLCHECKED;
        map[1][1] |= (CHECKED | MOUSE);
        map[1][2] |= CHECKED;
        map[1][3] |= CHECKED;
    }
}


void Maze::Read()
{
    for(int i=sizeCal-1; i>=0; i--)
    {
        for(int j=0; j<sizeCal; j++)
        {
            cin >> map[j][i];
        }
    }

    map[mouse->endX][mouse->endY] = 0;
}

void Maze::WriteOut()
{
    for(int i=sizeCal-1; i>=0; i--)
    {
        for(int j=0; j<sizeCal; j++)
        {
            cout << map[j][i] << " ";
        }
        cout << endl;
    }
}

void Maze::MarkVisited()
{
    map[mouse->posX][mouse->posY] |= VMOUSE;
}
