#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <symulator.h>
#include <drawingapi.h>
#include <maze.h>
#include <mouse.h>


#include <QFileDialog>
#include <QTextStream>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Ui::MainWindow *ui;

    Mouse *mouse;
    Maze *mazeFull;
    Maze *maze;
    Solver *solver;

    DrawingAPI *previewAPI;
    DrawingAPI *realAPI;

    int waitTime=300;
    int timerId;
    bool wF;
    bool start;
    bool pause;

    void UpdateMat();
    void UpdateSteps();
    void Reset();
    bool WallCheck(int x, int y);
    bool CheckN(int x, int y, int special);
    bool CheckE(int x, int y, int special);
    bool CheckS(int x, int y, int special);
    bool CheckW(int x, int y, int special);

private slots:
    void on_LoadButton_clicked();
    void on_ResetButton_clicked();
    void on_comboBox_activated(const QString &x);
    void on_speedSelector_sliderMoved(int position);
    void on_StartButton_clicked();
    void on_PauseButton_clicked();

protected:
    void timerEvent(QTimerEvent *event);
};

#endif // MAINWINDOW_H
