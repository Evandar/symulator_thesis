#include "solverastarimp.h"
#include <iomanip>
#include <opencv2/opencv.hpp>

using namespace std;


SolverAStarImp::SolverAStarImp(Maze *inputMaze)
{
    maze = inputMaze;

    prev = new PII*[maze->sizeCal];
    for(int i = 0; i < maze->sizeCal; i++)
        prev[i] = new PII[maze->sizeCal];

    costG = new int*[maze->sizeCal];
    vis = new bool*[maze->sizeCal];
    for(int i = 0; i < maze->sizeCal; i++)
    {
        costG[i] = new int[maze->sizeCal];
        vis[i] = new bool[maze->sizeCal];
    }

    tempOrient = 0;
    returnStatus = 0;
}

int SolverAStarImp::RealCost(PII from, PII acc, PII to)
{
    PII vecU;
    if(from == acc)
        vecU = PII(avMoves[maze->mouse->orientation], avMoves[maze->mouse->orientation+1]);
    else vecU = SubPII(acc, from);
    PII vecV = SubPII(to, acc);

    double vectorProduct = Det(vecV, vecU);
    double scalarProduct = Dot(vecV, vecU);

    if(vectorProduct == 0)
    {
        if(scalarProduct < 0)
            return BACK_WEIGHT;
        else
            return STRAIGHT_WEIGHT;
    }

    if(scalarProduct > 0)
        return TURN_WEIGHT;
    else if (scalarProduct == 0)
        return 2 * TURN_WEIGHT;
    return 3 * TURN_WEIGHT;
}

int SolverAStarImp::HeuristicCost(int x, int y, int gX, int gY)
{
    if(gX == -1 && gY == -1) return 0;
    int a = gX - x;
    int b = gY - y;
    return sqrt(a*a + b*b);//abs(a) + abs(b);
}

void SolverAStarImp::Clean(int x)
{
    for(int i = 0; i < maze->sizeCal; i++)
    {
        for(int j = 0; j < maze->sizeCal; j++)
        {
            vis[i][j] = 0;
            prev[i][j] = PII(-1,-1);
            costG[i][j] = INF;
            maze->map[i][j] &= ~TRACK;
            if(x) maze->map[i][j] &= ~FASTEST_TRACK;
        }
    }
    tempOrient = maze->mouse->orientation;
    h.size = 0;
}

bool SolverAStarImp::ComputeAlgorithm()
{
    Clean(1);

    if(GoalEx(maze->mouse)) return 1;
    AStar(maze->mouse->startX, maze->mouse->startY, maze->mouse->endX, maze->mouse->endY);
    ShowFastest(maze->mouse->endX, maze->mouse->endY);

    Clean(0);
    AStar(maze->mouse->posX, maze->mouse->posY, -1, -1);
    DetermineMoves(0, maze->mouse->gX, maze->mouse->gY, tempOrient);
    return 0;
}

void SolverAStarImp::AStar(int x, int y, int gX, int gY)
{

    prev[x][y] = PII(x,y);
    costG[x][y] = 0;

    Vertex temp = Vertex(x,y,HeuristicCost(x, y, gX, gY));
    h.Push(temp);

    while(h.size)
    {
        temp = h.Pop();
        x = temp.x;
        y = temp.y;
        vis[x][y] = 1;

        if(gX == -1 && gY == -1)
        {
            if((maze->map[x][y] & FASTEST_TRACK) && !(maze->map[x][y] & CHECKED))
            {
                maze->mouse->gX = x;
                maze->mouse->gY = y;
                return;
            }
        }
        else
        {
            if(x == gX && y == gY)
                return;
        }


        for(int i = 0; i < 8; i += 2)
        {
            int toX = x + avMoves[i];
            int toY = y + avMoves[i+1];

            int sumG = costG[x][y] + RealCost(prev[x][y], PII(x,y), PII(toX, toY));
            if(vis[toX][toY])
                continue;
            if(costG[toX][toY] > sumG && !(maze->map[toX][toY] & WALL))
            {
                prev[toX][toY] = PII(x,y);
                costG[toX][toY] = sumG;
                h.Push(Vertex(toX,toY, costG[toX][toY] + HeuristicCost(toX, toY, gX, gY)));
            }
        }

    }

    maze->mouse->gX = maze->mouse->startX;
    maze->mouse->gY = maze->mouse->startY;
}


void SolverAStarImp::DetermineMoves(bool fake, int x, int y, int &tempOrient)
{
    PII curField = PII(x,y);
    PII prevField = prev[x][y];
    maze->map[curField.ST][curField.ND] |= TRACK;

    if(costG[curField.ST][curField.ND] == 0)
        return;
    DetermineMoves(fake, prevField.ST, prevField.ND, tempOrient);

    if(maze->map[prevField.ST][prevField.ND] & CHECKED)
    {
        PII vecU = SubPII(curField, prevField);
        PII vecV = PII(avMoves[tempOrient], avMoves[tempOrient+1]);

        double vectorProduct = Det(vecV, vecU);
        double scalarProduct = Dot(vecV, vecU);

        if(vectorProduct == 0)
        {
            if(scalarProduct < 0)
            {
                if(!fake) maze->mouse->actionQ.AddTurn(4);
                tempOrient -= 4;
            }
        }
        if(scalarProduct == 0)
        {
            if(vectorProduct < 0)
            {
                if(!fake) maze->mouse->actionQ.AddTurn(-2);
                tempOrient-=2;
            }
            else
            {
                if(!fake) maze->mouse->actionQ.AddTurn(2);
                tempOrient+=2;
            }
            if(fake)
            {
                maze->mouse->turns++;
            }
        }

        if(!fake)
            maze->mouse->actionQ.AddMove(1);
        else
            maze->mouse->moves++;

        if(tempOrient < 0) tempOrient += 8;
        if(tempOrient >= 8) tempOrient -= 8;
    }
}

void SolverAStarImp::ShowFastest(int x, int y)
{
    PII curField = PII(x, y);
    PII prevField = prev[x][y];

    if(costG[curField.ST][curField.ND] == 0)
        return;

    maze->map[curField.ST][curField.ND] |= FASTEST_TRACK;
    ShowFastest(prevField.ST, prevField.ND);

    return;
}

void SolverAStarImp::OptimalRoute()
{
    Clean(1);
    AStar(maze->mouse->startX, maze->mouse->startY,
          maze->mouse->endX, maze->mouse->endY);

    ShowFastest(maze->mouse->endX, maze->mouse->endY);

    tempOrient = maze->mouse->orientation;
    maze->mouse->moves = 0;
    maze->mouse->turns = 0;
    DetermineMoves(1, maze->mouse->endX, maze->mouse->endY, tempOrient);
}
