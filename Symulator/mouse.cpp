#include "mouse.h"
#include <iostream>

using namespace std;

Mouse::Mouse(int sX, int sY, int eX, int eY, OrientationNum orient)
{
    startX = sX;
    startY = sY;
    posX = sX;
    posY = sY;
    endX = eX;
    endY = eY;
    gX = endX;
    gY = endY;
    orientation = orient;
    steps = 0;
    moves = 0;
    turns = 0;
    optimalCost = 0;
}

void Mouse::ProcessAQ()
{
    if(actionQ.Status() == 1)
        return;

    ActionPack *temp = &actionQ.aQ[actionQ.qR];

    switch(temp->type)
    {
    case MOVE:
        ProcessMove(temp->arg);
        break;
    case PIVOT:
        ProcessTurn(temp->arg);
        break;
    case PIVOT_180:
        ProcessTurn(temp->arg);
        break;
    case CALIBRATE:
//        ProcessCalibrate();
        break;
    }

    temp->status = DONE;
    actionQ.qR++;
    if(actionQ.qR == ACTIONQ_SIZE)
        actionQ.qR = 0;
}



void Mouse::ProcessMove(int arg)
{
    switch (orientation)
    {
        case N:
            posY++;
            break;
        case E:
            posX++;
            break;
        case S:
            posY--;
            break;
        case W:
            posX--;
            break;
    }
    steps++;
}

using namespace std;
void Mouse::ProcessTurn(int arg)
{
    int temp = orientation;

    temp += (arg);
    if(temp < 0) temp += 8;
    if(temp >= 8) temp -= 8;

    orientation = static_cast<OrientationNum>(temp);
}





