#include <opencv2/opencv.hpp>

#include <main.h>
#include <symulator.h>
#include <QApplication>

#include <mainwindow.h>

using namespace std;
using namespace cv;


PII SubPII(PII a, PII b) {return PII(a.ST-b.ST, a.ND-b.ND);}
double Dot(PII u, PII v) {return u.ST * v.ST + u.ND * v.ND;}
double Det(PII u, PII v) {return u.ST * v.ND - v.ST * u.ND;}


int main(int argc, char *argv[])
{

    QApplication prog(argc, argv);
    MainWindow CMD;
    CMD.show();

    return prog.exec();
}
