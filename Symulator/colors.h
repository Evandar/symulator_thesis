#ifndef COLORS_H
#define COLORS_H

#include <opencv2/opencv.hpp>

class Colors
{
public:
    Colors();

    cv::Scalar white;
    cv::Scalar black;
    cv::Scalar gray;
    cv::Scalar red;
    cv::Scalar blue;
    cv::Scalar darkGreen;
};

#endif // COLORS_H
