#include "solver.h"

Solver::Solver()
{
}

int Solver::Prev(PII x)
{
    return (x.ST<<6) | x.ND;
}

PII Solver::Prev(int x)
{
    int mask = 0b111111;
    return PII((x>>6) & mask, x & mask);
}

bool Solver::Goal(Mouse *mouse)
{
    if ((mouse->posX >= mouse->gX-1 && mouse->posX <= mouse->gX+1)
            && (mouse->posY >= mouse->gY-1 && mouse->posY <= mouse->gY+1)) return 1;
    return 0;
}

bool Solver::Goal(int x, int y, Mouse *mouse)
{
    if( (x >= mouse->gX - 1 && x <= mouse->gX + 1)
            && (y >= mouse->gY - 1 && y <= mouse->gY + 1)) return 1;
    return 0;
}

bool Solver::GoalEx(Mouse *mouse)
{
    if( mouse->posX == mouse->gX && mouse->posY == mouse->gY)
        return 1;
    return 0;
}

bool Solver::GoalEx(int x, int y, Mouse *mouse)
{
    if( x == mouse->gX && y == mouse->gY)
        return 1;
    return 0;
}

