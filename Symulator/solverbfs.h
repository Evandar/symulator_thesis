#ifndef SOLVERBFS_H
#define SOLVERBFS_H

#include <main.h>
#include <solver.h>
#include <maze.h>

class SolverBFS : public Solver
{
public:
    SolverBFS(Maze *inputMaze);
    ~SolverBFS(){}


    Maze *maze;
    int avMoves[8] = {0,1,-1,0,0,-1,1,0};

    PII **prev;
    PII q[BFS_Q_SIZE];

    int tempOrient;
    int qW;
    int qR;
    bool returnStatus;
    bool av;

    void Clean();
    bool ComputeAlgorithm();
    void BFS(int x, int y);
    void DetermineMoves(bool fake, int x, int y, int &tempOrient);
    bool CheckPath(int x, int y);
    void ShowFastest(int x, int y);
    void OptimalRoute();
};

#endif // SOLVERBFS_H
