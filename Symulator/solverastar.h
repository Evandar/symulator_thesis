#ifndef SOLVERASTAR_H
#define SOLVERASTAR_H

#include <main.h>
#include <solver.h>
#include <maze.h>
#include <heap.h>

class SolverAStar : public Solver
{
public:
    SolverAStar(Maze *inputMaze);
    ~SolverAStar(){}

    Maze *maze;
    int avMoves[16] = {0,1, -1,0, 0,-1, 1,0,};

    PII **prev;
    int **costG;
    bool **vis;
    Heap h;

    int tempOrient;
    int returnStatus;
    bool av;

    void Clean();
    bool ComputeAlgorithm();
    void AStar(int x, int y, int gX, int gY);
    int RealCost(PII from, PII acc, PII to);
    int HeuristicCost(int x, int y, int gX, int gY);
    void DetermineMoves(bool fake, int x, int y, int &tempOrient);
    bool CheckPath(int x, int y);
    void ShowFastest(int x, int y);
    void OptimalRoute();
};

#endif // SOLVERASTAR_H
