#include "main.h"
#include "motion.h"

int cordsNS[10] = {0,1, 0,2, 1,2, 2,2, 2,3};
int cordsEW[10] = {1,0, 2,0, 2,1, 2,2, 3,2};


void Move()
{

}

void Turn(int x)
{

}


bool CheckN(int x, int y, int special)
{
    int ex = 0;
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze[x+i][y] & CHECKED))
            ex = 1;
        maze[x][y+i] |= mazeFull[x][y+i];
        maze[x][y+i] |= CHECKED;
        if(mazeFull[x][y+i] == WALLCHECKED)
            break;
    }

    if(special)
    {
        for(int i=0; i<10; i+=2)
        {

            int tox = x + cordsNS[i];
            int toy = y + cordsNS[i+1];

            if(!(maze[tox][toy] & CHECKED))
                ex = 1;
            maze[tox][toy] |= mazeFull[tox][toy];
            maze[tox][toy] |= CHECKED;
            if(mazeFull[tox][toy] == WALLCHECKED)
                break;

        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x - cordsNS[i];
            int toy = y + cordsNS[i+1];

            if(!(maze[tox][toy] & CHECKED))
                ex = 1;
            maze[tox][toy] |= mazeFull[tox][toy];
            maze[tox][toy] |= CHECKED;
            if(mazeFull[tox][toy] == WALLCHECKED)
                break;

        }
    }
    return ex;
}

bool CheckE(int x, int y, int special)
{
    bool ex = 0;

    // glowny
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze[x+i][y] & CHECKED))
            ex = 1;
        maze[x+i][y] |= mazeFull[x+i][y];
        maze[x+i][y] |= CHECKED;
        if(mazeFull[x+i][y] == WALLCHECKED)
            break;
    }

    if(special)
    {
        for(int i=0; i<10; i+=2)
        {
            int tox = x + cordsEW[i];
            int toy = y + cordsEW[i+1];

            if(!(maze[tox][toy] & CHECKED))
                ex = 1;
            maze[tox][toy] |= mazeFull[tox][toy];
            maze[tox][toy] |= CHECKED;
            if(mazeFull[tox][toy] == WALLCHECKED)
                break;
        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x + cordsEW[i];
            int toy = y - cordsEW[i+1];

            if(!(maze[tox][toy] & CHECKED))
                ex = 1;
            maze[tox][toy] |= mazeFull[tox][toy];
            maze[tox][toy] |= CHECKED;
            if(mazeFull[tox][toy] == WALLCHECKED)
                break;
        }
    }
    return ex;
}

bool CheckS(int x, int y, int special)
{
    bool ex = 0;
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze[x][y-i] & CHECKED))
            ex = 1;
        maze[x][y-i] |= mazeFull[x][y-i];
        maze[x][y-i] |= CHECKED;
        if(mazeFull[x][y-i] == WALLCHECKED)
            break;
    }

    if(special)
    {
        for(int i=0; i<10; i+=2)
        {

            int tox = x - cordsNS[i];
            int toy = y - cordsNS[i+1];

            if(!(maze[tox][toy] & CHECKED))
                ex = 1;
            maze[tox][toy] |= mazeFull[tox][toy];
            maze[tox][toy] |= CHECKED;
            if(mazeFull[tox][toy] == WALLCHECKED)
                break;

        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x + cordsNS[i];
            int toy = y - cordsNS[i+1];

            if(!(maze[tox][toy] & CHECKED))
                ex = 1;
            maze[tox][toy] |= mazeFull[tox][toy];
            maze[tox][toy] |= CHECKED;
            if(mazeFull[tox][toy] == WALLCHECKED)
                break;

        }
    }
    return ex;
}

bool CheckW(int x, int y, int special)
{
    bool ex = 0;
    for(int i=1; i<RANGE; i++)
    {
        if(!(maze[x-i][y] & CHECKED))
            ex = 1;
        maze[x-i][y] |= mazeFull[x-i][y];
        maze[x-i][y] |= CHECKED;

        if(mazeFull[x-i][y] == WALLCHECKED)
            break;
    }
    if(special)
    {
        for(int i=0; i<10; i+=2)
        {
            int tox = x - cordsEW[i];
            int toy = y + cordsEW[i+1];

            if(!(maze[tox][toy] & CHECKED))
                ex = 1;
            maze[tox][toy] |= mazeFull[tox][toy];
            maze[tox][toy] |= CHECKED;
            if(mazeFull[tox][toy] == WALLCHECKED)
                break;
        }

        for(int i=0; i<10; i+=2)
        {
            int tox = x - cordsEW[i];
            int toy = y - cordsEW[i+1];

            if(!(maze[tox][toy] & CHECKED))
                ex = 1;
            maze[tox][toy] |= mazeFull[tox][toy];
            maze[tox][toy] |= CHECKED;
            if(mazeFull[tox][toy] == WALLCHECKED)
                break;
        }
    }
    return ex;
}

bool WallCheck(int x, int y)
{
    maze[x][y] |= CHECKED;

    bool ex = 0;
    switch(orientation)
    {
        case N:
            ex += CheckN(x,y,1);
            ex += CheckW(x,y,0);
            ex += CheckE(x,y,0);
            break;

        case E:
            ex += CheckE(x,y,1);
            ex += CheckN(x,y,0);
            ex += CheckS(x,y,0);
            break;

        case S:
            ex += CheckS(x,y,1);
            ex += CheckE(x,y,0);
            ex += CheckW(x,y,0);
            break;

        case W:
            ex += CheckW(x,y,1);
            ex += CheckN(x,y,0);
            ex += CheckS(x,y,0);
            break;
    }

    maze[INIT_ENDX][INIT_ENDY] = CHECKED;
    return ex;
}

