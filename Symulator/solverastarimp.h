#ifndef SOVLERASTARIMP_H
#define SOVLERASTARIMP_H

#include <main.h>
#include <solver.h>
#include <maze.h>
#include <heap.h>

typedef enum
{
    BEST,
    MYPOS,
}ModeNum;


class SolverAStarImp : public Solver
{
public:
    SolverAStarImp(Maze *inputMaze);
    ~SolverAStarImp(){}

    Maze *maze;
    int avMoves[8] = {0,1, -1,0, 0,-1, 1,0,};

    PII **prev;
    int **costG;
    bool **vis;
    Heap h;

    PII tempGoal;
    int tempOrient;
    int returnStatus;

    void Clean(int x);
    bool ComputeAlgorithm();
    void AStar(int x, int y, int gX, int gY);
    int RealCost(PII from, PII acc, PII to);
    int HeuristicCost(int x, int y, int gX, int gY);
    void DetermineMoves(bool fake, int x, int y, int &tempOrient);
    void ShowFastest(int x, int y);
    void OptimalRoute();
};

#endif // SOVLERASTARIMP_H
