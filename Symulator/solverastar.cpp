#include "solverastar.h"
#include <iomanip>
#include <opencv2/opencv.hpp>

using namespace std;


SolverAStar::SolverAStar(Maze *inputMaze)
{
    maze = inputMaze;

    prev = new PII*[maze->sizeCal];
    for(int i = 0; i < maze->sizeCal; i++)
        prev[i] = new PII[maze->sizeCal];

    costG = new int*[maze->sizeCal];
    vis = new bool*[maze->sizeCal];
    for(int i = 0; i < maze->sizeCal; i++)
    {
        costG[i] = new int[maze->sizeCal];
        vis[i] = new bool[maze->sizeCal];
    }

    returnStatus = 0;
}



void SolverAStar::Clean()
{
    for(int i = 0; i < maze->sizeCal; i++)
    {
        for(int j = 0; j < maze->sizeCal; j++)
        {
            vis[i][j] = 0;
            prev[i][j] = PII(-1,-1);
            costG[i][j] = INF;
            maze->map[i][j] &= ~TRACK;
            maze->map[i][j] &= ~FASTEST_TRACK;
        }
    }
    tempOrient = maze->mouse->orientation;
    h.size = 0;
    av=1;
}

bool SolverAStar::ComputeAlgorithm()
{
    Clean();

    if(returnStatus)
    {
        if(GoalEx(maze->mouse))
        {
            maze->mouse->gX = maze->mouse->endX;
            maze->mouse->gY = maze->mouse->endY;

            AStar(maze->mouse->startX, maze->mouse->startY, maze->mouse->endX, maze->mouse->endX);
            returnStatus = 0;
            ShowFastest(maze->mouse->endX, maze->mouse->endY);
            return CheckPath(maze->mouse->endX, maze->mouse->endY);
        }
    }
    else if(Goal(maze->mouse))
    {
        maze->mouse->gX = maze->mouse->startX;
        maze->mouse->gY = maze->mouse->startY;
        returnStatus = 1;
    }


    AStar(maze->mouse->posX, maze->mouse->posY, maze->mouse->gX, maze->mouse->gY);
    ShowFastest(maze->mouse->gX, maze->mouse->gY);
    DetermineMoves(0, maze->mouse->gX, maze->mouse->gY, tempOrient);
    return 0;
}


int SolverAStar::RealCost(PII from, PII acc, PII to)
{
    PII vecU;
    if(from == acc)
        vecU = PII(avMoves[maze->mouse->orientation], avMoves[maze->mouse->orientation+1]);
    else vecU = SubPII(acc, from);
    PII vecV = SubPII(to, acc);

    double vectorProduct = Det(vecV, vecU);
    double scalarProduct = Dot(vecV, vecU);

    if(vectorProduct == 0)
    {
        if(scalarProduct < 0)
            return BACK_WEIGHT;
        else
            return STRAIGHT_WEIGHT;
    }

    if(scalarProduct > 0)
        return TURN_WEIGHT;
    else if (scalarProduct == 0)
        return 2 * TURN_WEIGHT;
    return 3 * TURN_WEIGHT;
}

int SolverAStar::HeuristicCost(int x, int y, int gX, int gY)
{
    int a = gX - x;
    int b = gY - y;
    return sqrt(a*a + b*b);
}

void SolverAStar::AStar(int x, int y, int gX, int gY)
{
    prev[x][y] = PII(x,y);
    costG[x][y] = 0;

    Vertex temp = Vertex(x,y,HeuristicCost(x,y,gX,gY));
    h.Push(temp);

    while(h.size)
    {
        temp = h.Pop();
        x = temp.x;
        y = temp.y;
        vis[x][y] = 1;


        if(x == gX && y == gY){
            break;
        }
        for(int i = 0; i < 8; i += 2)
        {
            int toX = x + avMoves[i];
            int toY = y + avMoves[i+1];

            int sumG = costG[x][y] + RealCost(prev[x][y], PII(x,y), PII(toX, toY));
            if(vis[toX][toY])
                continue;
            if(costG[toX][toY] > sumG && !(maze->map[toX][toY] & WALL))
            {
                prev[toX][toY] = PII(x,y);
                costG[toX][toY] = sumG;
                h.Push(Vertex(toX,toY, costG[toX][toY] + HeuristicCost(toX, toY, gX, gY)));
            }
        }
    }
}


void SolverAStar::DetermineMoves(bool fake, int x, int y, int &tempOrient)
{
    PII curField = PII(x,y);
    PII prevField = prev[x][y];


    if(costG[curField.ST][curField.ND] == 0)
        return;

    DetermineMoves(fake, prevField.ST, prevField.ND, tempOrient);

    if(maze->map[prevField.ST][prevField.ND] & CHECKED && av)
    {
        maze->map[curField.ST][curField.ND] |= TRACK;

        PII vecU = SubPII(curField, prevField);
        PII vecV = PII(avMoves[tempOrient], avMoves[tempOrient+1]);

        double vectorProduct = Det(vecV, vecU);
        double scalarProduct = Dot(vecV, vecU);

        if(vectorProduct == 0)
        {
            if(scalarProduct < 0)
            {
                if(!fake) maze->mouse->actionQ.AddTurn(4);
                tempOrient -= 4;
            }
        }
        if(scalarProduct == 0)
        {
            if(vectorProduct < 0)
            {
                if(!fake) maze->mouse->actionQ.AddTurn(-2);
                tempOrient-=2;
            }
            else
            {
                if(!fake) maze->mouse->actionQ.AddTurn(2);
                tempOrient+=2;
            }
            if(fake)
            {
                maze->mouse->turns++;
            }
        }

        if(!fake)
            maze->mouse->actionQ.AddMove(1);
        else
            maze->mouse->moves++;

        if(tempOrient < 0) tempOrient += 8;
        if(tempOrient >= 8) tempOrient -= 8;

    }
    else
        av = 0;
}

bool SolverAStar::CheckPath(int x, int y)
{
    PII curField = PII(x,y);
    PII prevField = prev[x][y];

    if(costG[curField.ST][curField.ND] == 0)
        return 1;
    if(maze->map[curField.ST][curField.ND] & CHECKED)
        return CheckPath(prevField.ST, prevField.ND);

    return 0;
}

void SolverAStar::ShowFastest(int x, int y)
{
    PII curField = PII(x, y);
    PII prevField = prev[x][y];

    if(prev[x][y] == curField)
        return;

    maze->map[curField.ST][curField.ND] |= FASTEST_TRACK;
    ShowFastest(prevField.ST, prevField.ND);

    return;
}

void SolverAStar::OptimalRoute()
{
    Clean();
    AStar(maze->mouse->startX, maze->mouse->startY,
          maze->mouse->endX, maze->mouse->endY);

    ShowFastest(maze->mouse->endX, maze->mouse->endY);
    tempOrient = maze->mouse->orientation;
    maze->mouse->moves = 0;
    maze->mouse->turns = 0;
    DetermineMoves(1, maze->mouse->endX, maze->mouse->endY, tempOrient);
}
