#ifndef UTILS_H
#define UTILS_H

#include "main.h"

void ReadMaze(int (&mazeFull)[MAZE_SIZE_CAL][MAZE_SIZE_CAL]);
void InitMaze(int (&maze)[MAZE_SIZE_CAL][MAZE_SIZE_CAL]);
void MarkVisited(int posX, int posY);
void RemoveMouse(int posX, int posY);
void SetMouse(int posX, int posY);
#endif // UTILS_H
