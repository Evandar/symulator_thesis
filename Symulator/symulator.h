#ifndef SYMULATOR_H
#define SYMULATOR_H

#include <main.h>
#include <maze.h>
#include <mouse.h>
#include <drawingapi.h>
#include <solver.h>
#include <solverwallfollower.h>
#include <solverbfs.h>
#include <solverastar.h>
#include <solverastarimp.h>

class Symulator
{
public:
    Symulator(int size, int sX, int sY, int eX, int eY, OrientationNum orient, MethodNum method);

    Mouse *mouse;
    Maze *mazeFull;
    Maze *maze;

    DrawingAPI *drawingAPI;
    Solver *solver;

    void Brute();
    bool SearchRun();




    //WORLD
    bool WallCheck(int x, int y);
    bool CheckN(int x, int y, int special);
    bool CheckE(int x, int y, int special);
    bool CheckS(int x, int y, int special);
    bool CheckW(int x, int y, int special);
};

#endif // SYMULATOR_H
